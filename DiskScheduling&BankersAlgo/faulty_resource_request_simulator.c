#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <semaphore.h>

//global variables
int *avail;
int **max, **hold, **need;
int proNum, resNum;


void* deadlockChecker(){

    int i, j, e;
    while(1){
        printf("Checking for deadlocks\n");
        e = 0;
        for(i=0;i<proNum;i++){
            for(j=0;j<resNum;j++){
                if(need[i][j]>avail[j]){
                    e=1;
                }
            }
        }
        if (e==1){
            printf("Deadlock will occur as processes request more resources, exiting...\n");
            exit(-1);
        }
        //runs every 10 seconds
        sleep(10);
    }
}

void* fault_simulator(){
    while(1){
        //random resources and random prob (0/1)
        int numb = rand()%(resNum+1);
        int prob = rand()%2;

        printf("Simulating fault, avail[%d] will decremented by %d\n", numb, prob);
        if(prob==1){
            avail[numb]--;
        }
        //runs every 10 seconds
        sleep(10);
    }
}

int isSafe(){
    //initialize temporary availability vectors
    int i, j, temp=0;
    int *work = (int*)malloc(resNum*sizeof(int));
    int *finish = (int*)malloc(proNum*sizeof(int));
    for(j=0;j<resNum;j++){
        work[j] = avail[j];
    }
    for(i=0;i<proNum;i++){
        finish[i] = 0;
    }

    //step 2
    int loop=1, loopExit=1;
    while(loop==1){
        loopExit=1;
        for(i=0;i<proNum;i++){
            if(finish[i] == 0){
                //loopExit--;
                for(j=0;j<resNum;j++){
                    if(need[i][j] <= work[j]){
                        loopExit--;
                        //step 3
                        work[j] += hold[i][j];
                        finish[i]=1;
                        //printf("process %d is done\n", i);
                    }
                    else finish[i]=0;
                }
            }

        }
        if(loopExit==1){
            //go to step 4
            loop=0;
        }
    }

    //step 4
    int done=1;
    for(i=0;i<proNum;i++){
        if (finish[i]!=1){
            done--;
        }
    }
    if(done==1){
        //printf("safe!\n");
        return 1;
    }
    else{
        printf("not safe!\n");
        return 0;
    }
}

int bankersAlgorithm(int i, int* req){

    //set variables
    int k, l;

    printf("Requesting resources for process %d\n", i);
    printf("The Resource vector requested array is : ");
    for(k=0;k<resNum;k++){
        printf("%d ",req[k]);
    }
    printf("\n");

    /*
    printf("The Available Resources array for process %d : ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",avail[k]);
        }
    printf("\n");

    printf("%d, need: ", i);
    for(k=0;k<resNum;k++){
        printf("%d ",need[i][k]);
    }
    printf("\n\n");*/

    int loop=1, loopExit=1, loop2=1;

    while(loop2==1){
        while(loop==1){
            for(k=0;k<resNum;k++){
                if(req[k]>need[i][k]){
                    printf("error\n");
                    exit(-1);
                }
                if(req[k]>avail[k]){
                    //printf("p:%d wait\n", i);
                    //sleep(3);
                    loopExit--;
                }
            }
            if(loopExit==1){
                //printf("p:%d continue\n", i);
                //go to step 3
                loop=0;
            }
        }
        //printf("p:%d continued\n", i);

        //step 3
        for(k=0;k<resNum;k++){
            avail[k] -= req[k];
            hold[i][k] += req[k];
            need[i][k] -= req[k];
        }


        /*printf("%d, step2 avail: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",avail[k]);
        }
        printf("\n");
        printf("%d, step2 hold: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",hold[i][k]);
        }
        printf("\n");
        printf("%d, step2 need: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",need[i][k]);
        }
        printf("\n\n");*/

        printf("Checking if allocation is safe\n");
        if(isSafe()==1){
            printf("System is safe : allocating\n");
            loop2=0;
            sleep(3);
        }
        else{
            printf("System is not safe : cancelling\n");
            for(k=0;k<resNum;k++){
            avail[k] += req[k];
            hold[i][k] -= req[k];
            need[i][k] += req[k];
            }
            //sleep(1);
        }
        printf("\n");
    }


        /*printf("%d, step3 avail: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",avail[k]);
        }
        printf("\n");
        printf("%d, step3 hold: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",hold[i][k]);
        }
        printf("\n");
        printf("%d, step3 need: ", i);
        for(k=0;k<resNum;k++){
            printf("%d ",need[i][k]);
        }
        printf("\n\n");
    printf("p:%d exited\n", i);*/

    return 0;
}


void* threadSimulator(void* p){
    int proID = *(int*) p;
    int i, j;

    //request resources
    int* req = malloc(resNum*sizeof(int));
    //initialize variables
    for(i=0;i<resNum;i++){
        req[i] = rand()%(max[proID][i]+1);
        hold[proID][i] = 0;
        need[proID][i] = max[proID][i];
        //printf("%d\n", need[proID][i]);
    }

    int loop = 1, done;
    while(loop==1){

        //check if a process is done
        done = 1;
        for(i=0;i<resNum;i++){
            if(need[proID][i]!=0){
                done--;
            }
        }

        if(done==1){
            loop=0;
            printf("Realeasing resources of process %d\n", proID);
            for(i=0;i<resNum;i++){
                //Releases resources if it gets its needs met
                avail[i] += 2*hold[proID][i];
            }
        }
        else{
            //if not done, call banker's algorithm again
            for(i=0;i<resNum;i++){
                req[i] = rand()%(need[proID][i]+1);
            }
            bankersAlgorithm(proID, req);
        }
    }

}

int main(){

    //N processes
    printf("Input the number of processes\n");
    scanf("%d", &proNum);

    //M resources
    printf("Input the number of district resources\n");
    scanf("%d", &resNum);


    int i, j;
    avail = (int*)malloc(resNum*sizeof(int));

    max = (int**)malloc(proNum*sizeof(int*));
    hold = (int**)malloc(proNum*sizeof(int*));
    need = (int**)malloc(proNum*sizeof(int*));
    for (i=0; i<proNum; i++){
        max[i] = (int*)malloc(resNum*sizeof(int));
        hold[i] = (int*)malloc(resNum*sizeof(int));
        need[i] = (int*)malloc(resNum*sizeof(int));
    }


    //Availability vector
    printf("Input the amount of each resources in the system\n");
    for(i=0;i<resNum;i++){
        scanf("%d", &avail[i]);
    }


    //N*M matrix
    printf("Input the maximum resources claim per process / resource\n");
    for(i=0;i<proNum;i++){
        for(j=0;j<resNum;j++){
            scanf("%d", &max[i][j]);
        }
    }


    printf("The Allocated Resources table is :\n");
    for(i=0;i<proNum;i++){
        for(j=0;j<resNum;j++){
            printf("0 ");
        }
        printf("\n");
    }
    printf("\n");

    printf("The Maximum Claim table is :\n");
    for(i=0;i<proNum;i++){
        for(j=0;j<resNum;j++){
            printf("%d ", max[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    //Create threads simulating processes
    pthread_t tid[proNum];

    //create a thread that takes away resources from the available pool (fault_simulator)
    pthread_create(&tid[0], NULL, fault_simulator, NULL);

    //create a deadlockchecker
    pthread_create(&tid[1], NULL, deadlockChecker, NULL);

    //Attaching function and lauching threads
    for(i=0;i<proNum;i++){
        int num = i+2;
        pthread_create(&tid[num], NULL, threadSimulator, &i);
        sleep(1);
    }

}

