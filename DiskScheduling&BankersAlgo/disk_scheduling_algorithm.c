#include <stdio.h>  //for printf and scanf
#include <stdlib.h> //for malloc

#define LOW 0
#define HIGH 199
#define START 53

//compare function for qsort
//you might have to sort the request array
//use the qsort function
// an argument to qsort function is a function that compares 2 quantities
//use this there.
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

//function to swap 2 integers
void swap(int *a, int *b)
{
    if (*a != *b)
    {
        *a = (*a ^ *b);
        *b = (*a ^ *b);
        *a = (*a ^ *b);
        return;
    }
}

//Prints the sequence and the performance metric
void printSeqNPerformance(int *request, int numRequest)
{
    int i, last, acc = 0;
    last = START;
    printf("\n");
    printf("%d", START);
    for (i = 0; i < numRequest; i++)
    {
        printf(" -> %d", request[i]);
        acc += abs(last - request[i]);
        last = request[i];
    }
    printf("\nPerformance : %d\n", acc);
    return;
}

//access the disk location in FCFS
void accessFCFS(int *request, int numRequest)
{
    //simplest part of assignment
    printf("\n----------------\n");
    printf("FCFS :");
    printSeqNPerformance(request, numRequest);
    printf("----------------\n");
    return;
}

//access the disk location in SSTF
void accessSSTF(int *request, int numRequest)
{
    //write your logic here
    printf("\n----------------\n");
    printf("SSTF :");

    //set variables
    int last = START;
    int i, j;
    int tempp, tempn, num;

    //find ss
    for(i=0; i<numRequest; i++){
        tempn = HIGH+LOW+1;
        tempp = i;
        for(j=i; j<numRequest; j++){
            num = abs(last-request[j]);
            if(num<tempn){
                tempn=num;
                tempp=j;
            }
        }
        int t = request[i];
        request[i] = request[tempp];
        request[tempp] = t;
        last = request[i];
    }

    printSeqNPerformance(request, numRequest);
    printf("----------------\n");
    return;
}

//access the disk location in SCAN
void accessSCAN(int *request, int numRequest)
{

	//write your logic here
    printf("\n----------------\n");
    printf("SCAN :");

    //0=left, 1=right
    int direction;
    if((START-LOW)<(HIGH-START)) direction=0; else direction=1;
    int* newRequest = malloc((numRequest+1) * sizeof(int));

    int i, j;
    int tempp, tempn, num;
    int position=-1;

    //sort the array
    for(i=0; i<numRequest; i++){
        tempn = HIGH+LOW+1;
        tempp = i;
        for(j=i; j<numRequest; j++){
            num = request[j];
            if(num<tempn){
                tempn=num;
                tempp=j;
            }
        }
        int t = request[i];
        request[i] = request[tempp];
        request[tempp] = t;

        //get the position < START
        if(request[i]<START){
            position = i;
        }
    }

    //go left first
    if(direction==0){
        i = position;
        j = 0;
        while(i>=0){
            newRequest[i]=request[j];
            i--;
            j++;
        }

        newRequest[j]=LOW;
        j++;

        while(j<numRequest+1){
            newRequest[j]=request[j-1];
            j++;
        }
    }

    //go right first
    if(direction==1){
        i = 0;
        j = position+1;

        while(j<numRequest){
            newRequest[i]=request[j];
            i++;
            j++;
        }

        newRequest[i] = HIGH;
        i++;

        j = position;
        while(j>=0){
            newRequest[i]=request[j];
            j--;
            i++;
        }

    }

    printSeqNPerformance(newRequest, numRequest+1);
    printf("----------------\n");
    return;
}

//access the disk location in CSCAN
void accessCSCAN(int *request, int numRequest)
{
    //write your logic here
    printf("\n----------------\n");
    printf("CSCAN :");

    //0=left, 1=right
    int direction;
    if((START-LOW)<(HIGH-START)) direction=0; else direction=1;
    int* newRequest = malloc((numRequest+2) * sizeof(int));

    int i, j;
    int tempp, tempn, num;
    int position=-1;

    //sort the array
    for(i=0; i<numRequest; i++){
        tempn = HIGH+LOW+1;
        tempp = i;
        for(j=i; j<numRequest; j++){
            num = request[j];
            if(num<tempn){
                tempn=num;
                tempp=j;
            }
        }
        int t = request[i];
        request[i] = request[tempp];
        request[tempp] = t;

        //get the position < START
        if(request[i]<START){
            position = i;
        }
    }

    //go left first
    if(direction==0){
        i = position;
        j = 0;
        while(i>=0){
            newRequest[i]=request[j];
            i--;
            j++;
        }

        newRequest[j]=LOW;
        newRequest[j+1]=HIGH;
        j+=2;

        int i = numRequest-1;
        while(j<numRequest+2){
            newRequest[j]=request[i];
            j++;
            i--;
        }
    }

    //go right first
    if(direction==1){
        i = 0;
        j = position+1;

        while(j<numRequest){
            newRequest[i]=request[j];
            i++;
            j++;
        }

        newRequest[i]=HIGH;
        newRequest[i+1]=LOW;
        i+=2;

        j = 0;
        while(j<=position){
            newRequest[i] = request[j];
            i++;
            j++;
        }
    }


    printSeqNPerformance(newRequest, numRequest+2);
    printf("----------------\n");
    return;
}

//access the disk location in LOOK
void accessLOOK(int *request, int numRequest)
{
    //write your logic here
    printf("\n----------------\n");
    printf("LOOK :");

    //0=left, 1=right
    int direction;
    if((START-LOW)<(HIGH-START)) direction=0; else direction=1;
    int* newRequest = malloc(numRequest * sizeof(int));

    int i, j;
    int tempp, tempn, num;
    int position=-1;

    //sort the array
    for(i=0; i<numRequest; i++){
        tempn = HIGH+LOW+1;
        tempp = i;
        for(j=i; j<numRequest; j++){
            num = request[j];
            if(num<tempn){
                tempn=num;
                tempp=j;
            }
        }
        int t = request[i];
        request[i] = request[tempp];
        request[tempp] = t;

        //get the position < START
        if(request[i]<START){
            position = i;
        }
    }

    //go left first
    if(direction==0){
        i = position;
        j = 0;
        while(i>=0){
            newRequest[i]=request[j];
            i--;
            j++;
        }

        while(j<numRequest){
            newRequest[j]=request[j];
            j++;
        }
    }

    //go right first
    if(direction==1){
        i = 0;
        j = position+1;

        while(j<numRequest){
            newRequest[i]=request[j];
            i++;
            j++;
        }
        j = position;
        while(j>=0){
            newRequest[i]=request[j];
            j--;
            i++;
        }

    }

    printSeqNPerformance(newRequest, numRequest);
    printf("----------------\n");
    return;
}

//access the disk location in CLOOK
void accessCLOOK(int *request, int numRequest)
{
    //write your logic here
    printf("\n----------------\n");
    printf("CLOOK :");

    //0=left, 1=right
    int direction;
    if((START-LOW)<(HIGH-START)) direction=0; else direction=1;
    int* newRequest = malloc((numRequest+1) * sizeof(int));

    int i, j;
    int tempp, tempn, num;
    int position=-1;

    //sort the array
    for(i=0; i<numRequest; i++){
        tempn = HIGH+LOW+1;
        tempp = i;
        for(j=i; j<numRequest; j++){
            num = request[j];
            if(num<tempn){
                tempn=num;
                tempp=j;
            }
        }
        int t = request[i];
        request[i] = request[tempp];
        request[tempp] = t;

        //get the position < START
        if(request[i]<START){
            position = i;
        }
    }

    //go left first
    if(direction==0){
        i = position;
        j = 0;
        while(i>=0){
            newRequest[i]=request[j];
            i--;
            j++;
        }

        newRequest[j]=HIGH;
        j+=1;

        int i = numRequest-1;
        while(j<numRequest+1){
            newRequest[j]=request[i];
            j++;
            i--;
        }
    }

    //go right first
    if(direction==1){
        i = 0;
        j = position+1;

        while(j<numRequest){
            newRequest[i]=request[j];
            i++;
            j++;
        }

        newRequest[i]=LOW;
        i+=1;

        j = 0;
        while(j<=position){
            newRequest[i] = request[j];
            i++;
            j++;
        }
    }


    printSeqNPerformance(newRequest, numRequest+1);
    printf("----------------\n");
    return;
}

int main()
{
    int *request, numRequest, i,ans;

    //allocate memory to store requests
    printf("Enter the number of disk access requests : ");
    scanf("%d", &numRequest);
    request = malloc(numRequest * sizeof(int));

    printf("Enter the requests ranging between %d and %d\n", LOW, HIGH);
    for (i = 0; i < numRequest; i++)
    {
        scanf("%d", &request[i]);
    }

    /*numRequest = 6;
    request = malloc(numRequest * sizeof(int));
    request[0] = 25;
    request[1] = 128;
    request[2] = 198;
    request[3] = 69;
    request[4] = 54;
    request[5] = 88;*/

    printf("\nSelect the policy : \n");
    printf("----------------\n");
    printf("1\t FCFS\n");
    printf("2\t SSTF\n");
    printf("3\t SCAN\n");
    printf("4\t CSCAN\n");
    printf("5\t LOOK\n");
    printf("6\t CLOOK\n");
    printf("----------------\n");
    scanf("%d",&ans);

    switch (ans)
    {
    //access the disk location in FCFS
    case 1: accessFCFS(request, numRequest);
        break;

    //access the disk location in SSTF
    case 2: accessSSTF(request, numRequest);
        break;

        //access the disk location in SCAN
     case 3: accessSCAN(request, numRequest);
        break;

        //access the disk location in CSCAN
    case 4: accessCSCAN(request,numRequest);
        break;

    //access the disk location in LOOK
    case 5: accessLOOK(request,numRequest);
        break;

    //access the disk location in CLOOK
    case 6: accessCLOOK(request,numRequest);
        break;

    default:
        break;
    }
    return 0;
}
