/*
 ----------------- COMP 310/ECSE 427 Winter 2018 -----------------
 Dimitri Gallos
 Assignment 2 skeleton

 -----------------------------------------------------------------
 I declare that the awesomeness below is a genuine piece of work
 and falls under the McGill code of conduct, to the best of my knowledge.
 -----------------------------------------------------------------
 */

//Please enter your name and McGill ID below
//Name: Yihe Zhang
//McGill ID: 260738383


#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <semaphore.h>


int BUFFER_SIZE = 100; //size of queue

//initialize variables for semaphores
pthread_mutex_t mutex;
sem_t full;
sem_t empty;
sem_t pass;

//global variables
int GLOBOL = 0;
int SIG = 1;
int TOTAL = 0;

// A structure to represent a queue
struct Queue
{
    int front, rear, size;
    unsigned capacity;
    int* array;
};

// function to create a queue of given capacity.
// It initializes size of queue as 0
struct Queue* createQueue(unsigned capacity)
{
    struct Queue* queue = (struct Queue*) malloc(sizeof(struct Queue));
    queue->capacity = capacity;
    queue->front = queue->size = 0;
    queue->rear = capacity - 1;  // This is important, see the enqueue
    queue->array = (int*) malloc(queue->capacity * sizeof(int));
    return queue;
}

// Queue is full when size becomes equal to the capacity
int isFull(struct Queue* queue)
{
    return ((queue->size ) >= queue->capacity);
}

// Queue is empty when size is 0
int isEmpty(struct Queue* queue)
{
    return (queue->size == 0);
}

// Function to add an item to the queue.
// It changes rear and size
void enqueue(struct Queue* queue, int item)
{
    if (isFull(queue))
        return;
    queue->rear = (queue->rear + 1)%queue->capacity;
    queue->array[queue->rear] = item;
    queue->size = queue->size + 1;
    //printf("%d enqueued to queue\n", item);
}

// Function to remove an item from queue.
// It changes front and size
int dequeue(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    int item = queue->array[queue->front];
    queue->front = (queue->front + 1)%queue->capacity;
    queue->size = queue->size - 1;
    return item;
}

// Function to get front of queue
int front(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    return queue->array[queue->front];
}

// Function to get rear of queue
int rear(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    return queue->array[queue->rear];
}

void print(struct Queue* queue){
    if (queue->size == 0){
        return;
    }

    for (int i = queue->front; i < queue->front +queue->size; i++){

        printf(" Element at position %d is %d \n ", i % (queue->capacity ), queue->array[i % (queue->capacity)]);
    }
}

struct Queue* queue;

/*Producer Function: Simulates an Airplane arriving and dumping 5-10 passengers to the taxi platform */
void *FnAirplane(void* cl_id)
{
    //usleep(100);
    int airID = *(int *) cl_id;
    int i, j, ID, planeID;

    while(SIG){

        int passengers = rand()%6+5;
	i = 0;
	j = passengers;

	
        sem_wait(&pass);
        //critical section 1
        planeID = GLOBOL;
        GLOBOL+=1;
	if(SIG==0)break;
	if(GLOBOL>=TOTAL)GLOBOL=0;
        sem_post(&pass);
	
        //variable for passenger ID
        int passID = 1000000 + planeID*1000;

	printf("Airplane %d arrives with %d passengers\n", airID, passengers);

        while(i<j){
            if(isFull(queue)){
                printf("Platform is full: Rest of passengers of plane %d take the bus\n", airID);
                SIG = 0;
                break;
            }
            ID = passID+i;
            sem_wait(&full);
            pthread_mutex_lock(&mutex);
            //critical section 2
            enqueue(queue, ID);
            printf("Passenger %d of airplane %d arrives to platform\n", ID, airID);
            pthread_mutex_unlock(&mutex);
            sem_post(&empty);

            i++;
        }

    sleep(1);

    }
    //printf("%d\n", airid);
    //printf("Creating airplane thread %d\n", id);
}

/* Consumer Function: simulates a taxi that takes n time to take a passenger home and come back to the airport */
void *FnTaxi(void* pr_id)
{
    int taxiID = *(int *) pr_id;
    int passID;
    int exitfn = 0;

    while(1){
        printf("Taxi driver %d arrives\n", taxiID);
        while(isEmpty(queue)){
            printf("Taxi driver %d waits for passengers to enter the platform\n", taxiID);
            if(!SIG){
                exitfn = 1;
                pthread_exit(NULL);
            }
            sleep(1);
        }

        sem_wait(&empty);
        pthread_mutex_lock(&mutex);
	//critical section 3
        passID = dequeue(queue);
        pthread_mutex_unlock(&mutex);
        sem_post(&full);
        printf("Taxi driver %d picked up client %d from the platform\n", taxiID, passID);
        usleep(167+rand()%334*1000);
    }
}

int main(int argc, char *argv[])
{

  int num_airplanes;
  int num_taxis;

  num_airplanes=atoi(argv[1]);
  num_taxis=atoi(argv[2]);

  printf("You entered: %d airplanes per hour\n",num_airplanes);
  printf("You entered: %d taxis\n", num_taxis);

  TOTAL = num_airplanes;
  //initialize queue
  queue = createQueue(BUFFER_SIZE);

  //declare arrays of threads and initialize semaphore(s)
    pthread_t threads[BUFFER_SIZE];


    pthread_mutex_init(&mutex, NULL);
    sem_init(&full, 0, BUFFER_SIZE);
    sem_init(&empty, 0, 0);
    sem_init(&pass, 0, 1);

  //create arrays of integer pointers to ids for taxi / airplane threads
  int *taxi_ids[num_taxis];
  int *airplane_ids[num_airplanes];

  //create threads for airplanes
    int i = 0;

    for(i=0; i<num_airplanes; i++){
        //threads[i] = i;
        airplane_ids[i] = i;
        pthread_create(&threads[i], NULL, FnAirplane, &airplane_ids[i]);
        printf("Creating airplane thread %d \n", i);
    }


    //create threads for taxis
    int j;
    for(j=0; j<num_taxis; j++){
        taxi_ids[j] = j;
        pthread_create(&threads[j], NULL, FnTaxi, &taxi_ids[j]);
    }

  pthread_exit(NULL);
}
