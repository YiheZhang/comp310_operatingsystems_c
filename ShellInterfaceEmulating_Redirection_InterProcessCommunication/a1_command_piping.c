/*
----------------- COMP 310/ECSE 427 Winter 2018 -----------------
I declare that the awesomeness below is a genuine piece of work
and falls under the McGill code of conduct, to the best of my knowledge.
-----------------------------------------------------------------
*/

//Please enter your name and McGill ID below
//Name: Yihe Zhang
//McGill ID: 260738383

#include<stdio.h>
#include<unistd.h>
int main(){
    int fd[2];
    pipe(fd);
    char buffer[500];
    int save = dup(1);

    if(fork()==0){
        //Child: execute ls using execvp
        dup2(fd[1], 1);
        close(fd[1]);
        //redirect output
        char *argv[] = {"ls", 0};
        execvp(argv[0],argv);
    }else{
        //Parent: print output from ls here
        //read output from pipe, and then print
        read(fd[0], buffer, 500);
        printf("%s", buffer);
    }
    return 0;
}
