/*
----------------- COMP 310/ECSE 427 Winter 2018 -----------------
I declare that the awesomeness below is a genuine piece of work
and falls under the McGill code of conduct, to the best of my knowledge.
-----------------------------------------------------------------
*/

//Please enter your name and McGill ID below
//Name: Yihe Zhang
//McGill ID: 260738383

#include <fcntl.h>
#include<stdio.h>
#include<unistd.h>
int main(){
    printf("First : Print to stdout\n");

    //create an file and save stdout
    int des = open("redirect_out.txt", O_WRONLY|O_CREAT);
    int save = dup(1);

    //redirect stdout
    dup2(des, 1);
    close(des);
    printf("Second : Print to redirect_out.txt\n");

    //flush buffer and restore stdout
    fflush(stdout);
    dup2(save, 1);
    close(save);

    printf("Third : Print to stdout\n");
    return;
}
